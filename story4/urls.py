from django.conf.urls import url
from django.urls import path
from .views import *

urlpatterns = [
    path('', home, name = 'home'),
    path('learn-more/', learn, name = 'learn-more'),
    path('review/', bonus, name = 'bonus'),
]