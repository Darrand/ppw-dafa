from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, "Home.html")

def learn(request):
    return render(request, "learn-more.html")

def bonus(request):
    return render(request, "bonus.html")