from django.urls import path
from .views import *

urlpatterns = [
    path('submit/', result, name="result"),
    path('schedule/', schedule, name="schedule"),
    path('form/', form, name='form'),
    path('delete/', delete, name='delete'),
]
