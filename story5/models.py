from django.db import models

# Create your models here.
class Schedule(models.Model):
    date = models.DateField()
    time = models.TimeField()
    name = models.CharField(max_length = 100, default = "")
    place = models.CharField(max_length = 100, default = "")
    category = models.CharField(max_length = 100, default = "")

    def __str__(self):
        return self.name