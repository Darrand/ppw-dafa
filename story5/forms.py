from django import forms


class Date_Form(forms.Form):

    attrs = {"class" : "form-control"}

    date_attrs = {"class" : "form-control",
                 "type" : "date"}

    time_attrs = {"class" : "form-control",
                 "type" : "time"}
                 
    date = forms.DateField(label="Date", required= True, widget= forms.DateInput(attrs=date_attrs))
    time = forms.TimeField(label= "Time", required= True, widget= forms.TimeInput(attrs=time_attrs))
    name = forms.CharField(label = "Event Name", max_length= 100, required = True, widget= forms.TextInput(attrs=attrs))
    place = forms.CharField(label = "Event Place", max_length= 100, required = False,widget = forms.TextInput(attrs=attrs))
    category = forms.CharField(label = "Event Category", max_length = 100, required =False, widget = forms.TextInput(attrs=attrs))