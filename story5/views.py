from django.shortcuts import render
from .forms import Date_Form
from .models import Schedule
from django.http import HttpResponseRedirect

response = {}
# Create your views here.
def form(request):
    return render(request, "form.html", {"date_form" : Date_Form})

def schedule(request):
    return render(request, "result.html", {"date_result" : Schedule.objects.all()}) 

def result(request):
    form = Date_Form(request.POST or None) 
    if (request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name'] if request.POST['name'] != "" else "name unknown"
        response['date'] = request.POST['date'] if request.POST['date'] != "" else "date unknown"
        response['time'] = request.POST['time'] if request.POST['time'] != "" else "time unknown"
        response['place'] = request.POST['place'] if request.POST['place'] != "" else "place unknown"
        response['category'] = request.POST['category'] if request.POST['category'] != "" else "category unknown"
        schedule = Schedule(date = response['date']
                            , time = response['time'] 
                            , name = response['name']
                            , place = response['place']
                            , category = response['category'])
        schedule.save()
        return HttpResponseRedirect('/schedule/')
    else:
        return HttpResponseRedirect('/story4/')

def delete(request):
    Schedule.objects.all().delete()
    return HttpResponseRedirect('/schedule/')